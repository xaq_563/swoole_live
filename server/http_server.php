<?php
$http = new swoole_http_server("0.0.0.0",8811);

$http->set([
	'enable_static_handler' => true,
	'document_root' => "/var/www/html/develop/swoole_live/public/static",
    'worker_num' => 4
]);
$http->on('WorkerStart', function (swoole_server $server, $worker_id) {
    // 定义应用目录
    define('APP_PATH', __DIR__ . '/../application/');
    // 加载基础文件
    //require __DIR__ . '/../thinkphp/base.php';
    require __DIR__ . '/../thinkphp/start.php';
});
$http->on('request',function($request, $response) use ($http){

    //将swoole的request请求转成php原生
    $_SERVER = [];
    if(isset($request->server)) {
        foreach ($request->server as $k => $v) {
            $_SERVER[strtoupper($k)] = $v;
        }
    }
    if(isset($request->header)) {
        foreach ($request->header as $k => $v) {
            $_SERVER[strtoupper($k)] = $v;
        }
    }
    $_GET = [];
    if(isset($request->get)) {
        foreach ($request->get as $k => $v) {
            $_GET[$k] = $v;
        }
    }
    $_POST = [];
    if(isset($request->post)) {
        foreach ($request->post as $k => $v) {
            $_POST[$k] = $v;
        }
    }
    ob_start(); //用来打开输出缓冲
    // 执行应用并响应
    try{
        think\Container::get('app', [ __DIR__ . '/../application/' ])
            ->run()
            ->send();
    }catch (\Exception $e){
        echo $e->getMessage();
    }
    echo "-action-".request()->action().PHP_EOL;
    $res = ob_get_contents();
    ob_end_clean();

	$response->end($res);

	//$http->close();将http请求注销掉
});

$http->start();
