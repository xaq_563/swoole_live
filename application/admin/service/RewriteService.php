<?php
/**
 * 把数据生成对应的静态文件
 * RewriteService.php
 * 文件描述
 * Created on 2018/6/11 22:31
 * Create by xuanqiang
 */

namespace app\admin\service;

use think\Env;

class RewriteService
{
    /**
     *  将文件的json格式生成对应的json文件
     * @param $json
     * @return bool|int
     */
    public function toJson($json){

        
        return file_put_contents(Env::get("ROOT_PATH")."/json/banner.json",$json);
    }
}