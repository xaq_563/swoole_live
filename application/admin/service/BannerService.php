<?php
/**
 * BannerService.php
 * 文件描述
 * Created on 2018/6/11 22:31
 * Create by xuanqiang
 */

namespace app\admin\service;

use app\admin\model\Banner;

class BannerService
{
    /**
     * 从数据库里提取到对应的轮播图，并整理成json格式
     * @param int --num
     * @pram json
     */
    public function getBannerJson($num = 5){
        $list = Banner::all(function ($query) use ($num){
            $query->where('status',1)->limit($num)->order('order','desc');
        });
        return $list->toJson();
    }
}