<?php
/**
 * Redis.php
 * 文件描述
 * Created on 2018/4/7 17:55
 * Create by xuanqiang
 */

namespace app\common\lib;


class Redis
{
    /**
     * 验证码  redis key的前缀
     * @var string
     */
    public static $pre = "sms_";

    /**用户user pre
     * @var string
     */
    public static $userpre = 'user_';

    /**
     * 存储验证码 redis key
     * @param $phone
     * @return string
     */
    public static function smsKey($phone) {
        return self::$pre.$phone;
    }

    public static function userKey($phone) {
        return self::$userpre.$phone;
    }
}
