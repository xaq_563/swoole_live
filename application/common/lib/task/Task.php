<?php
/**
 * swoole里所有的task异步任务
 * Created on 2018/6/17 16:35
 * Create by xuanqiang
 */
namespace app\common\lib\task;


use app\common\lib\aliyun\AliSms;
use app\common\lib\Redis;
use app\common\lib\redis\Predis;

class Task
{
    /**异步发送验证码
     * @param $data
     */
    public function sendSms($data){
        try{
            $response = ((new AliSms())->sendSms($data['phone'],$data['code']));
        }catch (\Exception $exception){
            return false;
        }

        if($response['status'] == 1) {
            Predis::getInstance()->set(Redis::smsKey($data['phone']),$data['code'],config('redis.time_out'));
        }else{
            return false;
        }

       return true;
    }
}