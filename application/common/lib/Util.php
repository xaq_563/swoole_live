<?php
/**
 * Util.php
 * 文件描述
 * Created on 2018/4/7 16:48
 * Create by xuanqiang
 */

namespace app\common\lib;


class Util
{
    /**统一接口返回格式
     * @param $data
     * @param int $code
     * @param string $msg
     * @return \think\response\Json
     */
    static function  json_success($data,$code=0,$msg='操作成功'){
        $result = [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
        return json($result);
    }

   static function json_failed($code,$msg='',$data=[]){
        $result = [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
        return json($result);
    }
}