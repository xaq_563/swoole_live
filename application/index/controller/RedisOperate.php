<?php
/**
 * RedisOperate.php
 * 文件描述
 * Created on 2018/6/6 16:27
 * Create by xuanqiang
 */

namespace app\index\controller;


class RedisOperate extends \Redis
{
    protected static $_instance = null;

    public static function getInstance(){
        if(null === self::$_instance){
            self::$_instance = new self();
            $host = $_SERVER['MEMCACHE_HOST'];
            $port = $_SERVER['MEMCACHE_PORT'];
            self::$_instance->connect($host,$port);
        }
        return self::$_instance;
    }
}