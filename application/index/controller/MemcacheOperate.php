<?php
/**
 * MemcacheOpeate.php
 * 文件描述
 * Created on 2018/6/6 16:23
 * Create by xuanqiang
 */

namespace app\index\controller;


class MemcacheOperate extends \Memcache
{
    //实例
    protected static $_instance = null;

    public static function getInstance(){
        if(null === self::$_instance){
            self::$_instance = new self();
            $host = $_SERVER['MEMCACHE_HOST'];
            $port = $_SERVER['MEMCACHE_PORT'];
            self::$_instance->addServer($host, $port);
        }
        return self::$_instance;
    }
}