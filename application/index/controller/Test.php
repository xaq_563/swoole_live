<?php
/**
 * Test.php
 * 文件描述
 * Created on 2018/5/6 15:12
 * Create by xuanqiang
 */

namespace app\index\controller;


//防止类被继承
final class DBHelper{

    private static $instance = null;

    //构造器私有，防止类外部实例化
    private function __construct()
    {
        //连接数据库操作
    }

    //获取实例
    public static function getInstance(){
        if(!(self::$instance instanceof self)){
            self::$instance = new self();
        }
        return self::$instance;
    }

    //防止实例被克隆
    private function __clone()
    {
        // TODO: Implement __clone() method.
    }
}