<?php
/**
 * login.php
 * 文件描述
 * Created on 2018/4/7 16:04
 * Create by xuanqiang
 */

namespace app\index\controller;

use app\common\lib\aliyun\AliSms;
use app\common\lib\Redis;
use app\common\lib\redis\Predis;
use app\common\lib\Util;
use think\Controller;
use think\Exception;

class Login extends Controller
{
    /**
     * @title index
     * @description 发送验证码
     */
    public function send(){
        $phoneNum= intval($_GET['phone_num']);
        if(empty($phoneNum)){
            return Util::json_failed(config('code.error'),'手机号不能为空');
        }

        //生成一个随机数
        $code = rand(1000,9999);

        //onTask任务异步发短信
        $taskData = [
            'method' => 'sendSms',
            'data' => [
                'phone' => $phoneNum,
                'code' => $code
            ]
        ];
        $_POST['http_server']->task($taskData); //把数据抛给task任务

//        if($response['status'] == 1) {
//            //redis
//            try{
////                $redis = new \Swoole\Coroutine\Redis();
////                $redis->connect(config('redis.host'),config('redis.port'));
////                $redis->set(Redis::smsKey($phoneNum),$code,Config('redis.time_out'));
////                return Util::json_success([],config('code.success'),"验证码发送成功");
//                //采取异步redis的方式
//                $client = new \swoole_redis;
//                $client->connect(config('redis.host'), config('redis.port'), function (\swoole_redis $client, $result)  use ($phoneNum, $code){
//                    if ($result === false) {
//                        echo "connect to redis server failed.\n";
//                        return;
//                    }
//                    $client->set(Redis::smsKey($phoneNum),$code,function (\swoole_redis $client, $result){
//                        var_dump($result);
//                    });
//                });
//            }catch (\Exception $exception){
//                return Util::json_failed(config('code.error'),$exception->getMessage());
//            }
//
//        }else{
//            return Util::json_failed(config('code.error'),"验证码发送失败");
//        }
        return Util::json_success('短信发送成功');
    }

    public function index(){
        //get phone code
        $phoneNum = intval($_GET['phone_num']);
        $code = intval($_GET['code']);
        if(empty($phoneNum) || empty($code)){
            return Util::json_failed(config('code.error'),'phone or code is empty');
        }

        //redis code
        $redisCode = Predis::getInstance()->get(Redis::smsKey($phoneNum));

        if($redisCode == $code){
            //写入redis
            $data = [
                'user' => $phoneNum,
                'srcKey' => md5(Redis::userKey($phoneNum)),
                'time' => time(),
                'isLogin' => true
            ];

            Predis::getInstance()->set(Redis::userKey($phoneNum),$data);

            return Util::json_success($data,config('code.success'),'ok');
        }else{
            return Util::json_failed(config('code.error'),'login error');
        }

    }
}