<?php
/**
 * redis.php
 * 文件描述
 * Created on 2018/4/7 17:27
 * Create by xuanqiang
 */

return [
  'host' => '127.0.0.1',
  'port' => '6379',
  'time_out' => 120, //redis失效时间
  'out_time' => 5, //redis请求超时时间
];